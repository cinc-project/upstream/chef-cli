#!/bin/bash -e
#
# Author:: Lance Albertson <lance@osuosl.org>
# Copyright:: Copyright 2020-2025, Cinc Project
# License:: Apache License, Version 2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

export PATH="/opt/omnibus-toolchain/embedded/bin/:${PATH}"

source /home/omnibus/load-omnibus-toolchain.sh
set -x
gem build chef-cli.gemspec
VERSION="$(cat VERSION)"
gem push chef-cli-${VERSION}.gem --host https://rubygems.cinc.sh
